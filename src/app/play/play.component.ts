import { Component, OnInit } from '@angular/core';
import {PlayersService} from '../services/players.service';
import {GamesService} from '../services/games.service';
import {PlayerGamesService} from '../services/player-games.service';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.less']
})
export class PlayComponent implements OnInit {
  error;
  player1 = {id: 0};
  player2 = {id: 0};
  players;
  score1;
  score2;

  constructor(private playerService: PlayersService, private gameService: GamesService, private playerGamesService: PlayerGamesService) { }

  ngOnInit() {
    this.getPlayers().subscribe(res => this.players = res );
  }

  getPlayers() {
    return this.playerService.getPlayers();
  }

  choosePlayer(player, playerNum: number) {
    if (playerNum === 1) {
      this.player1 = player;
    } else {
      this.player2 = player;
    }
  }

  play() {
    if ((this.score1 >= 21 || this.score2 >= 21)
      && this.score1 >= 0 && this.score2 >= 0 && this.score1 !== this.score2
      && this.player1.id >= 1 && this.player2.id >= 1) {
      this.error = '';
      const game = {
        player1Id: this.player1.id,
        player2Id: this.player2.id,
        score1: this.score1,
        score2: this.score2,
      };

      this.gameService.setGame(game).subscribe((res: PlayerGame) => {
        console.log(res);
        const player1game = {
          gameId: res.id,
          playerId: this.player1.id,
          win: this.score1 > this.score2
        };

        const player2game = {
          gameId: res.id,
          playerId: this.player2.id,
          win: this.score2 > this.score1
        };

        this.playerGamesService.setPlayerGame(player1game).subscribe(p1 => console.log(p1));
        this.playerGamesService.setPlayerGame(player2game).subscribe();
      });
    } else {
      this.error = 'Your scores cannot be the same, you cannot have a negative score, ' +
        'you must select 2 players, and one score must be at least 21';
    }
  }

  setScore(score: number, player: number) {
    if (player === 1) {
      this.score1 = score;
    } else {
      this.score2 = score;
    }
  }

}

interface PlayerGame {
  id: number;
  gameId: number;
  playerId: number;
  win: boolean;
}
