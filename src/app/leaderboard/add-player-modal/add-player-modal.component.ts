import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {PlayersService} from '../../services/players.service';

@Component({
  selector: 'app-add-player-modal',
  templateUrl: './add-player-modal.component.html',
  styleUrls: ['./add-player-modal.component.less']
})
export class AddPlayerModalComponent implements OnInit {
  player = {firstname: '', lastname: ''};

  @Output() onPlayerCreated: EventEmitter<any> = new EventEmitter<any>();

  constructor(private playerService: PlayersService) { }

  ngOnInit() {
  }

  createPlayer() {
    console.log('player', this.player);
    this.playerService.setPlayer(this.player).subscribe(res => this.onPlayerCreated.emit(res) );
  }

}
