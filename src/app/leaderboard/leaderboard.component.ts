import { Component, OnInit } from '@angular/core';
import { PlayersService } from '../services/players.service';
import { GamesService } from '../services/games.service';
import { PlayerGamesService } from '../services/player-games.service';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.less'],
})
export class LeaderboardComponent implements OnInit {
  games;
  playerGames;
  players;

  constructor(private playerService: PlayersService, private gameService: GamesService, private playerGamesService: PlayerGamesService) { }

  ngOnInit() {
    this.generateLeaderboard();
  }

  addPlayer(player) {
    player.wins = 0;
    player.losses = 0;
    this.players.push(player);
  }

  generateLeaderboard() {
    // Wait for the for all the data to come back.
    forkJoin([this.getGames(), this.getPlayerGames(), this.getPlayers()]).subscribe(([games, playerGames, players]) => {
      this.games = games;
      this.playerGames = playerGames;
      this.players = players;

      this.players.forEach((player, index) => {
        const wins = this.playerGames.filter(g => g.playerId === player.id && g.win === true ).length;
        const losses = this.playerGames.filter(g => g.playerId === player.id && g.win === false ).length;

        this.players[index].wins = wins;
        this.players[index].losses = losses;
        this.players[index].winPercentage = wins / (losses + wins) * 100;
      });

      this.players.sort((a, b) =>  (a.winPercentage > b.winPercentage) ? -1 : ((b.winPercentage > a.winPercentage) ? 1 : 0) );
    });
  }

  getGames() {
    return this.gameService.getGames();
  }

  getPlayerGames() {
    return this.playerGamesService.getPlayerGames();
  }

  getPlayers() {
    return this.playerService.getPlayers();
  }

}

