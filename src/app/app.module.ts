import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { PlayComponent } from './play/play.component';
import { AddPlayerModalComponent } from './leaderboard/add-player-modal/add-player-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    LeaderboardComponent,
    PlayComponent,
    AddPlayerModalComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
