import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PlayerGamesService {

  constructor(private http: HttpClient) { }

  getPlayerGames() {
    return this.http.get('http://localhost:3000/api/PlayerGames');
  }

  setPlayerGame(playerGame) {
    return this.http.post('http://localhost:3000/api/PlayerGames', playerGame);
  }
}
