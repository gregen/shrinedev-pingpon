import { TestBed } from '@angular/core/testing';

import { PlayerGamesService } from './player-games.service';
import {beforeEach, describe, expect, it} from '@angular/core/testing/src/testing_internal';

describe('PlayerGamesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlayerGamesService = TestBed.get(PlayerGamesService);
    expect(service).toBeTruthy();
  });
});
