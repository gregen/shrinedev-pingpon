import { TestBed } from '@angular/core/testing';

import { GamesService } from './games.service';
import {beforeEach, describe, expect, it} from '@angular/core/testing/src/testing_internal';

describe('GamesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GamesService = TestBed.get(GamesService);
    expect(service).toBeTruthy();
  });
});
