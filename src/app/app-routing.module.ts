import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LeaderboardComponent} from './leaderboard/leaderboard.component';
import {PlayComponent} from './play/play.component';

const routes: Routes = [
  { path: '', redirectTo: '/play', pathMatch: 'full' },
  { path: 'leaderboard', component: LeaderboardComponent },
  { path: 'play', component: PlayComponent },
  { path: '**', redirectTo: '/play', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
