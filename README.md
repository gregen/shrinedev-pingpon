#CLIs

Run the following to get both CLIs installed  
`npm install -g @angular/cli`  
`npm install -g @loopback/cli`

Then run  
`npm install`  
to get the packages installed.

#Database

Run `mysql -u root -p` in the command line.

Then `create database pingpong;`

Followed by  
`grant all privileges on pingpong.* to 'pingpong'@'localhost' identified by 'tabletennis';`

Now `exit;` and run  
`mysql -u pingpong -tabletennis pingpong < populate-data.sql`

#Startup

`ng serve` will start the angular server.  
In a new terminal tab/window, run `npm start` to boot the Loopback server.  

Open your browser and navigate to localhost:4200

#Tests

`ng test` will run the unit test (make sure the servers aren't running)
