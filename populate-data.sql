insert into Player values (1, Aaron, Aaronson);
insert into Player values (1, Bob, Bobson);
insert into Player values (1, Charles, Charleston);
insert into Player values (1, David, Davidson);

insert into Game values (1, 1, 2, 21, 18);
insert into Game values (2, 1, 3, 21, 12);
insert into Game values (3, 1, 4, 21, 16);
insert into Game values (4, 2, 3, 21, 14);
insert into Game values (5, 2, 4, 20, 21);
insert into Game values (6, 3, 4, 15, 21);

insert into PlayerGame values (1, 1, 1, 1);
insert into PlayerGame values (2, 1, 2, 0);
insert into PlayerGame values (3, 2, 1, 1);
insert into PlayerGame values (4, 2, 3, 0);
insert into PlayerGame values (5, 3, 1, 1);
insert into PlayerGame values (6, 3, 4, 0);
insert into PlayerGame values (7, 4, 2, 1);
insert into PlayerGame values (8, 4, 3, 0);
